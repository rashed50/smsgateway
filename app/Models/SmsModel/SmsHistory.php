<?php

namespace App\Models\SmsModel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmsHistory extends Model
{
    use HasFactory;
    protected $table = 'sms_histories';
    protected $fillable =[
        'smsHistoryId',
        'companyNameId',
        'phone',
        'smsType',
        'msgBody',
        'smsCount',
        'smsDate',
    ];
}
 