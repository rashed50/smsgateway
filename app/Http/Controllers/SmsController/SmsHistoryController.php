<?php

namespace App\Http\Controllers\SmsController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SmsModel\SmsHistory;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Throwable;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Illuminate\Support\Str;

class SmsHistoryController extends Controller
{
    function sendSMS(Request $request){
		
       

        if(isset($_POST['phone'])  && isset($_POST['msgBody'])  && isset($_POST['smsType']) && isset($_POST['companyId']) )
        {    
                 // && isset($_POST['companyId'])
                $companyId = $_POST['companyId'];
                $phone = $_POST['phone'];
                $msgbody = $_POST['msgBody'];
                $smsType = $_POST['smsType'];
       
                $aSmsDetails = new SmsHistory;
                $aSmsDetails->companyNameId = $companyId;
                $aSmsDetails->phone = $phone;
                $aSmsDetails->smsType = $smsType;
                $aSmsDetails->msgBody = $msgbody;
                 
                $count = $length = Str::length($msgbody);
                if($smsType == 'text' || $smsType == 'Text'){
                     $count =  (int) (($count/121) + 1);
                }else {
                    $count =  (int) (($count/61) + 1);
                }
                $aSmsDetails->smsCount = $count;
                $aSmsDetails->smsDate = Carbon::now()->toDateString(); ;
        
                $res = $aSmsDetails->save();

                $apikey = 'starhair607fe476853fa7.85800013';
                $senderId = '8809612446121';
            
              $url = "http://bulksms.smsbuzzbd.com/smsapi";
              $data = [
                    "api_key" => $apikey,
                    "type" => $smsType,
                    "contacts" => $phone,
                    "senderid" => $senderId,
                    "msg" => $msgbody,
                  ];
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $url);
              curl_setopt($ch, CURLOPT_POST, 1);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              $response = curl_exec($ch);
              curl_close($ch);
              echo $response;
         
        }else 
            echo 'data not found';
    }
    
}
